//
//  Deck.h
//  Matchismo
//
//  Created by Francis San Juan on 2013-07-16.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"

@interface Deck : NSObject

- (void)addCard:(Card *)card atTop:(BOOL)atTop;
- (Card *)drawRandomCard;

@end
