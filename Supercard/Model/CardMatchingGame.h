//
//  CardMatchingGame.h
//  Matchismo
//
//  Created by Francis San Juan on 2013-07-16.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Card.h"
#import "Deck.h"

@interface CardMatchingGame : NSObject

// designated initializer
// there is no 'default' init for this class,
// everything must go through this initializer
- (id) initWithCardCount:(NSUInteger)count
               usingDeck:(Deck *)deck;

- (void)flipCardAtIndex:(NSUInteger)index;

- (Card *)cardAtIndex:(NSUInteger)index;

@property (readonly, nonatomic) int score;

@end
