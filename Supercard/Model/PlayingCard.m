//
//  PlayingCard.m
//  Matchismo
//
//  Created by Francis San Juan on 2013-07-16.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import "PlayingCard.h"

@interface PlayingCard()

@end

@implementation PlayingCard


// must synthesize b/c we've implemented both the getter and setter for this property
@synthesize suit = _suit;


-(NSString *)contents
{
  NSArray *rankString = [PlayingCard rankStrings];
  return [rankString[self.rank] stringByAppendingString:self.suit];
}

-(NSString *)suit
{
  // returns the suit if set,
  // otherwise returns ?
  return _suit ? _suit : @"?";
}

-(void)setSuit:(NSString *)suit
{
  // sets the suit only if it contains a string
  // that is a valid suit
  if ([[PlayingCard validSuits] containsObject:suit] ) {
    _suit = suit;
  }
}

-(void)setRank:(NSUInteger)rank
{
  // sets this card's rank only if it
  // is supplied with a valid rank value
  if ( rank <= [PlayingCard maxRank] ) {
    _rank = rank;
  }
}

// overrides Card's match: method
-(int)match:(NSArray *)otherCards {
  int score = 0;

  // only match if comparison is made between 1 card
  // and another
  if ([otherCards count] == 1) {
    id otherCard = [otherCards lastObject];
    if ([otherCard isKindOfClass:[PlayingCard class]]) {
      // cast to ensure card really is a playing card
      PlayingCard *otherPlayingCard = (PlayingCard *)otherCard;
      if ([otherPlayingCard.suit isEqualToString:self.suit]) {
        score = 1;
      } else if (otherPlayingCard.rank == self.rank) {
        score = 4;
      }
    }
  }

  return score;
}

+(NSArray *)validSuits
{
  return @[@"♥",@"♦",@"♠",@"♣"];
}

+(NSArray *)rankStrings
{
  return @[@"?", @"A", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"J", @"Q", @"K"];
}

+(NSUInteger)maxRank
{
  return [self.rankStrings count] - 1;
}

@end
