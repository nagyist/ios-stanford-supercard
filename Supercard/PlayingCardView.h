//
//  PlayingCardView.h
//  Supercard
//
//  Created by Francis San Juan on 2013-08-06.
//  Copyright (c) 2013 Francis San Juan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayingCardView : UIView

@property (nonatomic) NSUInteger rank;
@property (strong, nonatomic) NSString *suit;

@property (nonatomic) BOOL faceUp;

- (void)pinch:(UIPinchGestureRecognizer *)gesture;

@end
